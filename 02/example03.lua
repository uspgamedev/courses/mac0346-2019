
-- luacheck: no global

x = 1 + 3 * 23 / 29387

print(x)
print(print)

x = 2 ^ 0.5
print(x)

print(10 > 5)
print(7 == 7)
print(12 >= 7)
print(5 ~= 4)

print("asd" == "asd")
print("asd" == 123)

print(true or false)
print(true and false)
print(not false)

if "asd" or false then
  print("true")
end

function foo()
  print("foo")
  return 42
end

function bar()
  print("bar")
  return nil
end

print('---')
print(foo() or bar())
print('---')
print(bar() or foo())
print('---')
print(bar() and foo())
print('---')
print(foo() and bar())


function test(x)
  local alpha = "beta" -- luacheck: no unused
  return x and "foo" or "bar"
end

print(test(true))
print(test(false))

print(bob)

