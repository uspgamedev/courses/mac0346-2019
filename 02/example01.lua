
-- luacheck: no global

f = function()
  print("foo")
end

function g()
  print("bar")
end

f()
g()


t = {}

t.foo = function(self) print("oi", self) end

function t.bar(self)
  print("tchau", self)
end

function t:baz()
  print(self)
end

t.foo()
t.bar()
t:baz()


