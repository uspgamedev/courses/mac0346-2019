
-- luacheck: no global
--[[
alshda
askdakjshd
aksdhakj
]]

x = 42
a, b = 1, 2
print(a, b)
a, b = b, a
print(a, b)

function foo()
  return 2, 4
end

c, d = foo()
print(c, d)

x = 9

if x > 10 then
  print("maior")
elseif x < 10 then
  print("menor")
else
  print("igual")
end

c = 1
while c <= 10 do
  print(c)
  c = c + 1
end

c = 1
repeat
  print(c)
  c = c + 1
until c > 10

for i=1,10,2 do
  print(i)
end

bob = 1337

t = {}
for i=1,10 do
  t[i] = i*2
end
for i=1,10 do
  print(t[i])
end

for i,v in ipairs(t) do
  print(i, v)
end

t.x = 42
t.y = 1337

for k,v in pairs(t) do
  print(k, v)
end

for i=1,#t do
  print(i, t[i])
end

