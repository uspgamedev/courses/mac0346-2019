
-- luacheck: globals love

local last_dt

function love.update(dt)
  last_dt = dt
end

function love.draw()
  love.graphics.print("hello world", 0, 0)
  love.graphics.print(last_dt, 200, 200)
  love.graphics.print(love.timer.getFPS(), 200, 300)
end

