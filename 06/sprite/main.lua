
-- luacheck: globals love

local _sprite
local _canvas

local function _loadSprite()
  _sprite = {}
  _sprite.img = love.graphics.newImage("goblin spritesheet calciumtrice.png")
  _sprite.img:setFilter('nearest', 'nearest')
  _sprite.frames = {}
  _sprite.animation = {
    { i = 1, j = 1, duration = 0.200 },
    { i = 1, j = 3, duration = 0.100 },
    { i = 1, j = 1, duration = 0.700 },
  }
  for i=1,10 do
    _sprite.frames[i] = {}
    for j=1,10 do
      _sprite.frames[i][j] = love.graphics.newQuad(
        (j-1) * 32, (i-1) * 32, 32, 32, _sprite.img:getDimensions()
      )
    end
  end
  _sprite.current = 1
  _sprite.t0 = love.timer.getTime()
  _sprite.timer = 0
end

function love.load()
  _loadSprite()
  _canvas = love.graphics.newCanvas(love.graphics.getDimensions())
end

function love.update(_)
  local timer = love.timer.getTime() - _sprite.t0
  local duration = _sprite.animation[_sprite.current].duration
  if timer >= duration then
    _sprite.current = (_sprite.current % #_sprite.animation) + 1
    _sprite.t0 = love.timer.getTime() - (timer - duration)
  end
end


function love.draw()
  love.graphics.setCanvas(_canvas)
  love.graphics.push()
  love.graphics.clear()
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.setBlendMode("alpha")
  love.graphics.scale(4, 4)
  local frame = _sprite.current
  local i = _sprite.animation[frame].i
  local j = _sprite.animation[frame].j
  love.graphics.draw(_sprite.img, _sprite.frames[i][j],
                     100, 100, 0, 1, 1, 16, 32)
  love.graphics.rectangle('fill', 50, 100, 100, 20)
  love.graphics.pop()
  love.graphics.setCanvas()
  --love.graphics.setColor(1, 1, 1, 1)
  --love.graphics.setBlendMode("alpha", "premultiplied")
  love.graphics.draw(_canvas, 0, 0)
  love.graphics.draw(_canvas, 0, 0, 0, 1, -1)
end

