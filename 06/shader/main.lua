
-- luacheck: globals love

local _shader_code = [[
uniform float time;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords) {
  vec2 center = vec2(400, 300);
  float dist = distance(screen_coords, center);
  float radius = 200 + 50*sin(10*time);
  return vec4(1, 1, 1, 1 - step(radius, dist));
}
]]
local _shader
local _t0

function love.load()
  _shader = love.graphics.newShader(_shader_code)
  _t0 = love.timer.getTime()
end

function love.update(_)
  _shader:send("time", love.timer.getTime() - _t0)
end


function love.draw()
  love.graphics.setShader(_shader)
  love.graphics.rectangle('fill', 0, 0, love.graphics.getDimensions())
  love.graphics.setShader()
end

