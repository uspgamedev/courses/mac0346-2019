
-- luacheck: globals love


local _goblin_sheet
local _goblin_sprite

local function _newSprite(img, n, m)
  local w, h = img:getDimensions()
  local qw, qh = w/n, h/m
  local frames = {}
  for i = 1, m do
    frames[i] = {}
    for j = 1, n do
      frames[i] = love.graphics.newQuad((j-1) * qw, (i-1) * qh, qw, qh, w, h)
    end
  end
  return {
    img = img,
    frames = frames,
    current = 1,
    hotspot = {qw/2, qh}
  }
end

function love.load()
  _goblin_sheet = love.graphics.newImage("goblin spritesheet calciumtrice.png")
  _goblin_sheet:setFilter('nearest', 'nearest')
  _goblin_sprite = _newSprite(_goblin_sheet, 10, 10)
end

local function _drawSprite(sprite)
  love.graphics.draw(sprite.img, sprite.frames[sprite.current], 0, 0, 0, 1, 1,
                     unpack(sprite.hotspot))
end

function love.draw()
  local w, h = love.graphics.getDimensions()
  love.graphics.translate(w/2, h/2)
  love.graphics.scale(4, 4)
  love.graphics.rectangle('fill', -20, 0, 40, 20)
  _drawSprite(_goblin_sprite)
end

