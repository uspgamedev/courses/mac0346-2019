
-- luacheck: globals love

local PolySource = require 'polysource'

local _SAMPLE_RATE = 44100
local _DURATION = 1.0
local _DELAY = 0.3

local _sounddata
local _source
local _delay

local function _playSound()
  _source:play()
  _delay = _delay + _DELAY
end

function love.load()
  local n = _DURATION * _SAMPLE_RATE
  _sounddata = love.sound.newSoundData(n, _SAMPLE_RATE, 16, 1)
  for i = 0, n-1 do
    local t = i / _SAMPLE_RATE
    local amp = math.min(1, 2 ^ -(4*t - .3))
    local sample = amp * math.sin(440 * math.pi * 2 * t)
    sample = math.min(.9, math.max(-.9, sample))
    _sounddata:setSample(i, sample)
  end
  _source = PolySource(5, _sounddata)
  _delay = 0
  _playSound()
end

function love.update(dt)
  _delay = _delay - dt
  if _delay <= 0 then
    _playSound()
  end
end

