
-- luacheck: globals love

local class = require 'class'
local PolySource = class()

function PolySource:_init(n, sounddata)
  self.data = sounddata
  self.sources = {}
  self.n = n
  for i = 1, n do
    self.sources[i] = love.audio.newSource(sounddata)
  end
  self.next = 1
end

function PolySource:play()
  self.sources[self.next]:play()
  self.next = self.next % self.n + 1
end

return PolySource

