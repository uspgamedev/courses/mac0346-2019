
-- luacheck: globals love

local PolySource = require 'polysource'

local F = 44100
local T = 2.0
local D = 0.1

local _data
local _source
local _delay = 0

local function _play()
  --_source:stop()
  _source:play()
  _delay = _delay + D
end


function love.load()
  local n = T * F
  _data = love.sound.newSoundData(n, 44100, 16, 1)
  for i = 0, n-1 do
    local t = i / F
    local a = math.min(1, 2 ^ -(4*t - .3))
    local sample = a * math.sin(880 * 2 * math.pi * t)
    _data:setSample(i, sample)
  end
  _source = PolySource(5, _data)
  _play()
end

function love.update(dt)
  _delay = _delay - dt
  if _delay <= 0 then
    _play()
  end
end

