
-- luacheck: globals love

local Vec = require 'vec'


local _objects


function love.load()
  _objects = {}
  _objects[1] = { pos = Vec(400, 300), movement = Vec(20, -500) }
end

local function _updatePhysics(dt)
  for _,obj in ipairs(_objects) do
    obj.pos = obj.pos + obj.movement * dt
    obj.movement = obj.movement + Vec(0, 10)
  end
end

function love.update(dt)
  _updatePhysics(dt)
end

function love.draw()
  for _,obj in ipairs(_objects) do
    love.graphics.circle('fill', obj.pos.x, obj.pos.y, 32)
  end
end

