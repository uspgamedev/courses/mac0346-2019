
-- luacheck: globals love

local TWEEN = { objs = {} }

function TWEEN.go(obj, key, from, to, duration, callback)
  TWEEN.objs[obj] = {
    t = 0, key = key, from = from, to = to, duration = duration,
    callback = callback
  }
end

function TWEEN.update(dt)
  for obj,tween in pairs(TWEEN.objs) do
    tween.t = tween.t + dt
    local progress = math.min(1, tween.t / tween.duration)
    progress = 1 - math.abs(progress - 1)^3
    obj[tween.key] = tween.to * progress + tween.from * (1 - progress)
    if tween.t >= tween.duration then
      TWEEN.objs[obj] = nil
      if tween.callback then
        tween.callback()
      end
    end
  end
end

local _test

local _animation
function love.load()
  _test = { x = 100, y = 100 }
  _animation = coroutine.wrap(
    function()
      TWEEN.go(_test, 'x', 100, 200, 2, _animation)
      coroutine.yield()
      TWEEN.go(_test, 'y', 100, 300, 1, _animation)
      coroutine.yield()
      TWEEN.go(_test, 'x', 200, 500, 3)
    end
  )
  _animation()
end

function love.update(dt)
  TWEEN.update(dt)
end

function love.draw()
  love.graphics.circle('fill', _test.x, _test.y, 32)
end

