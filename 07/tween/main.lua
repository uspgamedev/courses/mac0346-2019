
-- luacheck: globals love

local TWEEN = { objs = {} }

function TWEEN.new(obj, prop, from, to, duration, callback)
  TWEEN.objs[obj] = {
    prop = prop,
    from = from,
    to = to,
    duration = duration,
    timer = 0,
    callback = callback
  }
end

function TWEEN.update(dt)
  for obj,tween in pairs(TWEEN.objs) do
    tween.timer = tween.timer + dt
    local progress = math.min(1, tween.timer / tween.duration)
    progress = 1 - math.abs(progress - 1) ^ 4
    obj[tween.prop] = tween.from * (1 - progress)
                    + tween.to * progress
    if tween.timer >= tween.duration then
      TWEEN.objs[obj] = nil
      if tween.callback then
        tween.callback()
      end
    end
  end
end

local _obj
local _animation

function love.load()
  local await = coroutine.yield
  _obj = { x = 100, y = 100 }
  _animation = coroutine.wrap(function ()
    TWEEN.new(_obj, 'x', 100, 300, 2, _animation)
    await()
    TWEEN.new(_obj, 'y', 100, 500, 3, _animation)
    await()
    TWEEN.new(_obj, 'x', 300, 400, 0.5)
  end)
  _animation()
end

function love.update(dt)
  TWEEN.update(dt)
end

function love.draw()
  love.graphics.circle('fill', _obj.x, _obj.y, 32)
end


