
-- luacheck: globals love

local Button = require 'button'

local _widgets

function love.load()
  _widgets = {}
  local button = Button("Click me!", 100, 100, 200, 40)
  _widgets[button] = true
  function button:onClicked()
    print("I'm " .. tostring(self))
  end
end

function love.update(dt)
  for widget in pairs(_widgets) do
    widget:update(dt)
  end
end

function love.draw()
  for widget in pairs(_widgets) do
    widget:draw()
  end
end

