
local Widget = require 'class' ()

function Widget:_init(x, y, w, h)
  self.x, self.y = x, y
  self.w, self.h = w, h
end

function Widget:getPosition()
  return self.x, self.y
end

function Widget:getDimensions()
  return self.w, self.h
end

function Widget:getBounds()
  return self.x, self.x + self.w, self.y, self.y + self.h
end

-- luacheck: no unused

function Widget:update(dt)
  -- abstract
end

function Widget:draw()
  -- abstract
end

return Widget

