
-- luacheck: globals love

local Widget = require 'widget'

local Button = require 'class' (Widget)

function Button:_init(text, x, y, w, h)
  self:super(x, y, w, h)
  self.text = love.graphics.newText(love.graphics.newFont(32))
  self.text:addf(text, w, 'center', 0, 0)
  self.hovered = false
  self.pressed = false
end

function Button:update(_)
  local x, y = love.mouse.getPosition()
  local l, r, t, b = self:getBounds()
  local was_hovered = self.hovered
  self.hovered = not (x < l or x > r or y < t or y > b)
  if love.mouse.isDown(1) then
    local was_pressed = self.pressed
    self.pressed = self.hovered
    if not was_pressed and was_hovered and self.pressed then
      self:onClicked()
    end
  else
    self.pressed = false
  end
end

function Button:onClicked() -- luacheck: no unused
  -- abstract
end

function Button:draw()
  local g = love.graphics
  if self.pressed then
    g.setColor(.1, .2, .6)
  else
    g.setColor(.1, .3, .8)
  end
  g.push()
  g.translate(self:getPosition())
  if self.hovered then
    g.push()
    g.translate(-self.w*.05, -self.h*.05)
    g.scale(1.1, 1.1)
  end
  g.rectangle('fill', 0, 0, self:getDimensions())
  g.setColor(1, 1, 1)
  if self.hovered then
    g.pop()
  end
  g.draw(self.text, 0, self.h/2 - self.text:getHeight() / 2)
  g.pop()
end

return Button

