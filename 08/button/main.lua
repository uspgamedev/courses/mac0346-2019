
-- luacheck: globals love

local Button = require 'button'

local _button

function love.load()
  _button = Button("click me!", 100, 100, 256, 64)
end

function love.mousepressed(x, y, button)
  if button == 1 and _button:isInside(x, y) then
    _button:press()
  end
end

function love.draw()
  _button:draw()
end

