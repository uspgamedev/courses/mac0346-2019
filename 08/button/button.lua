
-- luacheck: globals love

local class = require 'class'

local Button = class()

function Button:_init(text, x, y, w, h, callback)
  local font = love.graphics.newFont(32)
  self.text = love.graphics.newText(font)
  self.text:addf(text, w, 'center')
  self.x, self.y = x, y
  self.w, self.h = w, h
  self.callback = callback
end

function Button:getPosition()
  return self.x, self.y
end

function Button:getDimensions()
  return self.w, self.h
end

function Button:isInside(x, y)
  local bx, by = self:getPosition()
  local bw, bh = self:getDimensions()
  if x >= bx and x <= bx + bw and y >= by and y <= by + bh then
    return true
  end
  return false
end

function Button:press() -- luacheck: no unused
  print("pressed!")
end

function Button:draw()
  local g = love.graphics
  local hovered = self:isInside(love.mouse.getPosition())
  g.push()
  g.translate(self.x + self.w/2, self.y + self.h/2)
  if hovered then
    g.scale(1.1, 1.1)
  end
  if hovered and love.mouse.isDown(1) then
    g.setColor(0, .1, .4)
  else
    g.setColor(.1, .2, .8)
  end
  g.rectangle('fill', -self.w/2, -self.h/2, self:getDimensions())
  g.setColor(1, 1, 1)
  g.draw(self.text, -self.w/2, -self.text:getHeight() / 2)
  g.pop()
end

return Button

