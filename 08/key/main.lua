
-- luacheck: globals love

local _key = ""

function love.keypressed(key)
  _key = key
end

function love.joystickpressed(_, b)
  _key = b
end

function love.update(_)
end

function love.draw()
  love.graphics.scale(5, 5)
  love.graphics.print(_key, 10, 10)
  for _,joystick in ipairs(love.joystick.getJoysticks()) do
    love.graphics.print(joystick:getAxis(1), 10, 20)
    love.graphics.print(joystick:getHat(1), 10, 30)
  end
  local mousepos = ("%.1f, %.1f"):format(love.mouse.getPosition())
  love.graphics.print(mousepos, 10, 40)
end

