
-- luacheck: globals love


local _mesh

local _mesh_vtx = {
  { 0, -100, 0, 0, 1, 0, 0, 1 },
  { 80, 20, 0, 0, 0, 1, 0, 1 },
  { -80, 20, 0, 0, 0, 0, 1, 1 },
}

function love.load()
  _mesh = love.graphics.newMesh(_mesh_vtx, 'triangles',
                                'static')
  love.graphics.setBackgroundColor(.3, .3, .3)
end

function love.draw()
  local w, h = love.graphics.getDimensions()

  love.graphics.push()
  love.graphics.translate(w/2, h/2)
  love.graphics.setColor(1, 0, 0, 1)
  love.graphics.draw(_mesh, 0, 0)
  local angle = 90 * 2 * math.pi / 360

  love.graphics.push()
  love.graphics.translate(300, 0)
  love.graphics.rotate(angle)
  love.graphics.setColor(0, 0, 1, 1)
  love.graphics.draw(_mesh, 0, 0) --300, 0, angle, 1, 1)
  love.graphics.pop()

  love.graphics.push()
  love.graphics.setColor(0, 1, 0, 1)
  love.graphics.translate(0, 200)
  love.graphics.draw(_mesh, 0, 0)
  love.graphics.pop()

  love.graphics.pop()

  love.graphics.rectangle('line', 0, 0, 10, 10)
end

