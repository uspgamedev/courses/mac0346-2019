


local function foo(x)
  print(x)
end

foo(42)
print(getfenv(foo) == _G)

setfenv(foo, {})

print(getfenv(foo) == _G)
--foo(42)

local chunk = loadfile("data.lua")
setfenv(chunk, {})
local data = chunk()

print(data.x, data.y, data.name)


