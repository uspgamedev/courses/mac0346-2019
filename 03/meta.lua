
local t = { 2, 3 }

local meta = {}

setmetatable(t, meta)

function meta:__add(other)
  return { self[1] + other[1], self[2] + other[2] }
end

local x = t + t

print(unpack(x))

function meta:__call()
  print(unpack(self))
end

t()

function meta:__index(key)
  if key == 'x' then
    return rawget(self, 1)
  elseif key == 'y' then
    return rawget(self, 2)
  end
end

function meta:__newindex(key, value)
  if key == 'x' then
    rawset(self, 1, value)
  end
end

print(t.x)

local s = setmetatable({}, meta)

print(s.x)

s.x = 20

print(s.x, s[1])

