
-- luacheck: no global

function mistake(x)
  --t[1] = x
  return x+x
end

local ok, result = pcall(mistake, 42)

if not ok then
  print(result)
else
  print("resposta: " .. result)
end

--return error("duplicate item detected")

local function takeDamage(amount)
  assert(amount > 0, "negative damage")
end

takeDamage(10)
takeDamage(-10)

