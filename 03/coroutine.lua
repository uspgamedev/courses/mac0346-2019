
local yield = coroutine.yield

local function routine()
  print(1)
  print(2)
  yield()
  print(3)
end

local co = coroutine.create(routine)

print("before")
coroutine.resume(co)
print("after")
coroutine.resume(co)
print("after after")

local function spawnEnemy(name)
  print("A wild " .. name .. " has appeared!")
end

local function waves()
  for _ = 1,10 do
    spawnEnemy("slime")
    yield(10)
  end
  yield(true)
  for _ = 1,8 do
    spawnEnemy("slime")
    yield()
  end
  for _=1,4 do
    spawnEnemy("orc")
    yield()
  end
  yield(true)
end

local wave_co = coroutine.wrap(waves)

print("first wave")
while not wave_co() do print("wait") end
print("second wave")
while not wave_co() do print("wait") end





