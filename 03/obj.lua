
local Vec = {}

Vec.__index = Vec

function Vec.new(x, y)
  local new_vec = { x, y }
  setmetatable(new_vec, Vec)
  return new_vec
end

function Vec:print()
  print("(" .. self[1] .. ',' .. self[2] .. ')')
end

return Vec

