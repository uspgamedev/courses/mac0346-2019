
-- luacheck: no global

function love.keypressed(key)
  if key == 'a' then
    if isNearSwitch() then
      openDoor()
    end
  end
end

function awardXP(player, enemy)
  player.xp = player.xp + enemy.reward
  emitSignal("xp-gained", player.xp)
end

function love.load()
  connect("xp-gained", function (xp)
    if xp > threshold then
      levelUp(player, map)
    end
  end)
end

function love.update(dt)
  if isNearSwitch() and pressedAButton() then
    openDoor()
  end
  if playerTryAttack() and ableToAttack() then
    playerAttack()
    awardXP(player, enemy)
    for enemy in enemies do
      if enemy.hp <= 0 then
        enemy:die()
        awardXP(player, enemy)
      end
    end
  end
end

